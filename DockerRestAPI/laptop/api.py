# from flask import Flask
from flask_restful import Resource, Api
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import flask
import arrow
import acp_times
import config
import logging

# Instantiate the app
app = Flask(__name__)
api = Api(app)

#mongodb
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
#configuration
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY#from credentials

contador = 0#too keep track which query is being look up
db.tododb.delete_many({})#delete all info from the previos request
#index
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')
#empty form result
@app.route('/vacio')#in case submit has no data filled
def vacio():
    return render_template('vacio.html')

#error handler
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")#from project 4(with an acp_times.py improved)
def _calc_times():
    km = request.args.get('km', 999, type=float)#This block gets variables from the script in html
    dist_km = request.args.get('brevet_dist_km', 999, type=float)
    hour = request.args.get('begin_time')
    date = request.args.get('begin_date')
    tiempo_completo = "{} {}".format(date, hour)

    open_time = acp_times.open_time(km, dist_km, tiempo_completo)#send the parameter to the calculator.
    close_time = acp_times.close_time(km, dist_km, tiempo_completo)

    if km <= dist_km:#adds a message correct or incorrect to the website if the brevet is out of bounds
        notes = "Correct"
    else:
        notes = "Incorrect"

    result = {"open": open_time, "close": close_time, "notes": notes}#to js.
    return flask.jsonify(result=result)

################### LOGIC FOR THE DB ########################

###for displaying
@app.route('/display', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    if len(items) == 0:#
        return render_template('vacio.html')#if no items, show vacio
    else:
        return render_template('todo.html', items=items)

###submit###
@app.route('/submit', methods=['POST'])
def new():
    #values gather from the form
    db.tododb.delete_many({})
    open = request.form.getlist("open")
    close = request.form.getlist("close")
    km = request.form.getlist("km")
    millas = request.form.getlist("miles")
    notes = request.form.getlist("notes")

    lenght = 0#loop to check how many elements have the list
    for i in open:
        if i is not "":
            lenght+=1

    if lenght == 0:
        return redirect(url_for('vacio'))

    global contador
    # temporal = contador
    i = 0
    while i < lenght:#we could have parse the info directly request form but for some reason, during the first query, it wont show the very first data submitted, so we have to put the info first in a list and then passed to json
        item_doc = {
            # 'contador':contador,# to know which counter is
            'miles': millas[i],
            'km':km[i],
            'open': open[i],
            'close': close[i],
            'notes': notes[i]
        }


        db.tododb.insert_one(item_doc)#insert item to the db
        i+=1

    contador+=1
    return redirect(url_for('index'))

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

def list():
    items = {}
    _items = db.tododb.find()
    i = 0
    for x in _items:
        items[i] ={}
        items[i]['open'] = x['open']
        items[i]['close'] = x['close']
        i+=1
    return items

#the apis only recollect info for open and close times.
class ListAll(Resource):
    def get(self):
        return list()

class ListOpenOnly(Resource):
    def get(self):
        all = list()
        dic = {}
        i = 0
        for x in all:
            dic[i] = {}
            dic[i]['open'] = all[i]['open']
            i+=1
        return dic

class ListCloseOnly(Resource):
    def get(self):
        all = list()
        dic = {}
        i = 0
        for x in all:
            dic[i] = {}
            dic[i]['close'] = all[i]['close']
            i+=1
        return dic

class ListAllFormat(Resource):
    def get(self, format):
        items = {}
        result = ''
        _items = db.tododb.find()
        if format == "csv":
            for x in _items:
                result+="open: " +str(x['open'])+",close: "+str(x['close'])+','
            result = result[:-1]
        if format == "json":
            return list()
        return result
#when using csv format, the patter would be "open: {time}, close: {time}". The labels are for making easier to see what it is going on. For only open or close, only the times would be presented.
class ListOpenFormat(Resource):
    def get(self, format):
        result = ''
        _items = db.tododb.find()
        top = request.args.get('top')

        if format == "csv":
            if top is not None:
                i=0
                while i< int(top):
                    result+=str(_items[i]['open'])+","
                    i+=1
                result = result[:-1]

            else:
                for x in _items:
                    result+=str(x['open'])+","
                result = result[:-1]

        if format == "json":
            if top is not None:
                all = list()
                dic = {}
                i = 0
                while i<int(top):
                    dic[i] = {}
                    dic[i]['open'] = all[i]['open']
                    i+=1
                return dic
            else:
                all = list()
                dic = {}
                i = 0
                for x in _items:
                    dic[i] = {}
                    dic[i]['open'] = all[i]['open']
                    i+=1
                return dic

        return result

class ListCloseFormat(Resource):
    def get(self, format):
        result = ''
        _items = db.tododb.find()
        top = request.args.get('top')

        if format == "csv":
            if top != None:
                i=0
                while i< int(top):
                    result+=str(_items[i]['close'])+","
                    i+=1
                result = result[:-1]

            else:
                for x in _items:
                    result+=str(x['close'])+","
                result = result[:-1]

        if format == "json":
            if top != None:
                all = list()
                dic = {}
                i = 0
                while i<int(top):
                    dic[i] = {}
                    dic[i]['close'] = all[i]['close']
                    i+=1
                return dic
            else:
                all = list()
                dic = {}
                i = 0
                for x in _items:
                    dic[i] = {}
                    dic[i]['close'] = all[i]['close']
                    i+=1
                return dic

        return result
#routes for apis
api.add_resource(ListAll, '/listAll')
api.add_resource(ListOpenOnly, '/listOpenOnly')
api.add_resource(ListCloseOnly, '/listCloseOnly')
api.add_resource(ListAllFormat, '/listAll/<format>')
api.add_resource(ListOpenFormat, '/listOpenOnly/<format>')
api.add_resource(ListCloseFormat, '/listCloseOnly/<format>')



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=CONFIG.PORT, debug=True)
