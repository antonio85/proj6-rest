<html>
    <head>
        <title>Consumer program for Brevets</title>
        <style>
            h1{
              font-size: 30px;
              width:500px;
              margin: 0 auto;
              background: gray;
              text-align: center;

            }
            h2{
              font-size: 20px;
              background: black;
              color: white;
              text-align: left;

            }

        </style>
    </head>

    <body>
        <h1>Project 6: Examples of Restful requests uses.</h1>
        <h2>List of all open and close times. Default JSON. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll');
            $obj = json_decode($json);
            foreach ($obj as $l) {
                echo "<li>Open: $l->open</li>";
                echo "<li>Close: $l->close</li>";
            }
            ?>
        </ul>
        <h2>List of all open times. Default JSON. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly');
            $obj = json_decode($json);
            foreach ($obj as $l) {
                echo "<li>Open: $l->open</li>";
            }
            ?>
        </ul>
        <h2>List of all close times. Default JSON. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly');
            $obj = json_decode($json);
            foreach ($obj as $l) {
                echo "<li>Close: $l->close</li>";
            }
            ?>
        </ul>
        <h2>List of all times using csv format. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll/csv');
            echo $json;
            ?>
        </ul>
        <h2>List of all close times using csv format. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/csv');
            echo $json;
            ?>
        </ul>

        <h2>List of 3 first close times using csv format. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/csv?top=3');
            echo $json;
            ?>
        </ul>
        <h2>List of 4 first open times using JSON format. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/json?top=4');
            $obj = json_decode($json);
            
            foreach ($obj as $l) {
                echo "<li>Open: $l->open</li>";
            }
            ?>
        </ul>
        <h2>List of 5 first open times and close using JSON format. </h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll/json?top=5');
            $obj = json_decode($json);
            foreach ($obj as $l) {
                echo "<li>Open: $l->open</li>";
                echo "<li>Close: $l->close</li>";

            }
            ?>
        </ul>
    </body>
    <h2>List of 5 first open times and close using CSV format. </h2>
    <ul>
        <?php
        $json = file_get_contents('http://laptop-service:5000/listAll/csv?top=5');
        echo $json
        ?>
    </ul>
</body>
</html>
